﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DockX
{
    /// <summary>
    /// Interaction logic for MainDock.xaml
    /// </summary>=
    public partial class MainDock : Window
    {
        ObservableCollection<Icon> Icons { get; set; }
        public MainDock()
        {
            InitializeComponent();

            Icons = new ObservableCollection<Icon>();
            Icons.Add(new Icon() { Title = "1", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "2", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "3", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "4", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "5", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "6", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "7", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "8", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "9", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "10", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "11", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "12", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "13", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "14", Source = "https://via.placeholder.com/32" });
            Icons.Add(new Icon() { Title = "15", Source = "https://via.placeholder.com/32" });

            IconsList.ItemsSource = Icons;
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //this.Height = 32;
            //this.Width = this.Icons.Count * this.Height;

            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right / 2 - this.Width /2;
            this.Top = desktopWorkingArea.Bottom - this.Height;
        }

        private void ShowSettings(object sender, RoutedEventArgs e)
        {
            SettingsPage settingsPage = new SettingsPage();
            settingsPage.Show();
        }

        private void quitDock(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }

    public class UriToBitmapConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri((string)value, UriKind.Absolute);
            bitmapImage.EndInit();

            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
