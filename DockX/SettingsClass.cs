﻿namespace DockX
{
    class SettingsClass
    {
        private string _language;
        private bool _runStartup;
        private bool _lockItems;

        public string Language
        {
            get { return _language; }
            set
            {
                if (_language != value)
                {
                    _language = value;
                }
            }
        }

        public bool RunStartup
        {
            get { return _runStartup; }
            set
            {
                if (_runStartup != value)
                {
                    _runStartup = value;
                }
            }
        }
        public bool LockItems
        {
            get { return _lockItems; }
            set
            {
                if (_lockItems != value)
                {
                    _lockItems = value;
                }
            }
        }
    }
}
