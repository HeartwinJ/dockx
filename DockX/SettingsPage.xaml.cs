﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace DockX
{
    public partial class SettingsPage : Window, INotifyPropertyChanged
    {
        public SettingsPage()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void closeButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        private void closeButton_TouchDown(object sender, TouchEventArgs e)
        {
            Close();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
